package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionDTO;
import com.spideo.submission.model.Auction;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AuctionFormConverterTest {

    @Resource
    private AuctionConverter auctionConverter;

    @Test
    void convert() {
        Auction anAuction = new Auction();
        anAuction.setName("name1");
        anAuction.setDescription("description1");
        AuctionDTO anAuctionDTO = auctionConverter.convert(anAuction);
        assertEquals(anAuctionDTO.getName(),anAuction.getName());
        assertEquals(anAuctionDTO.getDescription(),anAuction.getDescription());

    }

    @Test
    void revertConvert() {

        AuctionDTO auctionDTO = new AuctionDTO();
        auctionDTO.setName("name1");
        auctionDTO.setDescription("description1");
        Auction anAuction = auctionConverter.revertConvert(auctionDTO);
        assertEquals(auctionDTO.getName(),anAuction.getName());
        assertEquals(auctionDTO.getDescription(),anAuction.getDescription());

    }
}