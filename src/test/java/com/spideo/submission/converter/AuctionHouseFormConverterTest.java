package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionHouseForm;
import com.spideo.submission.model.AuctionHouse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class AuctionHouseFormConverterTest{

    @Resource
    private AuctionHouseFormConverter auctionHouseFormConverter;

    @Test
    public void testAuctionHouseConversionTest()
    {
        AuctionHouseForm auctionHouseForm = new AuctionHouseForm();
        auctionHouseForm.setName("name1");
        AuctionHouse auctionHouse = auctionHouseFormConverter.revertConvert(auctionHouseForm);
        assertEquals(auctionHouse.getName(),auctionHouseForm.getName());

    }

}
