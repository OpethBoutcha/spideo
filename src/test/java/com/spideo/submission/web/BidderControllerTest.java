package com.spideo.submission.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spideo.submission.SubmissionApplication;
import com.spideo.submission.dto.BidderForm;
import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IBidderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.MOCK, classes={ SubmissionApplication.class })
public class BidderControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private IBidderService bidderService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test_bidder_api_is_available () throws Exception
    {
        mockMvc.perform(get("/api/bidder")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void test_bidder_creation_api () throws Exception
    {
        BidderForm bidderForm = new BidderForm();
        bidderForm.setName("Sarah");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(bidderForm);
        mockMvc.perform(post("/api/bidder")
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> assertNotNull(bidderService.findBidderByName("Sarah"),"the bidder was not found"));

    }

    @Test
    public void test_bidder_delete_api_with_success () throws Exception
    {
        Bidder bidder = new Bidder("Sarah");
        bidderService.createItem(bidder);
        mockMvc.perform(delete("/api/bidder/Sarah")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        assertThrows(ItemNotFoundException.class , () ->  bidderService.findBidderByName("Sarah"));
    }

    @Test
    public void test_bidder_delete_api_with_error () throws Exception
    {
        Bidder bidder = new Bidder("Sarah");
        bidderService.createItem(bidder);
        MvcResult result = mockMvc.perform(delete("/api/bidder/Mona")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();
        assertNotNull(bidderService.findBidderByName("Sarah"));
        String response = result.getResponse().getContentAsString();
        assertTrue(response.contains("Item not found ! please make sure you enter a valid item"));
    }
}