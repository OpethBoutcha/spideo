package com.spideo.submission.web;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spideo.submission.SubmissionApplication;
import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.Auction;
import com.spideo.submission.model.AuctionHouse;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IAuctionHouseService;
import com.spideo.submission.service.IAuctionService;
import com.spideo.submission.service.IBidService;
import com.spideo.submission.service.IBidderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.MOCK, classes={ SubmissionApplication.class })
public class BidControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private IBidderService bidderService;

    @Autowired
    private IBidService bidService;

    @Autowired
    private IAuctionService auctionService;

    @Autowired
    private IAuctionHouseService auctionHouseService;

    @BeforeEach
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test_successful_bid_on_auction() throws Exception
    {
        Bidder bidder = new Bidder("Arya");
        bidderService.createItem(bidder);
        AuctionHouse auctionHouse = new AuctionHouse("HouseTargerian");
        auctionHouseService.createItem(auctionHouse);
        Auction auction = new Auction();
        auction.setName("valerian_steal");
        auction.setDescription("most wanted material for swards");
        auction.setStartingPrice(BigDecimal.valueOf(100));
        auction.setCurrentPrice(BigDecimal.valueOf(100));
        auction.setStartTime(LocalDateTime.MIN);
        auction.setEndTime(LocalDateTime.MAX);
        auctionService.createAuctionForAuctionHouseName("HouseTargerian",auction);

        BidForm bidForm = new BidForm();
        bidForm.setBidder("Arya");
        bidForm.setAuction("valerian_steal");
        bidForm.setBidPrice(BigDecimal.valueOf(101));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(bidForm);

        mockMvc.perform(post("/api/bid")
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(result -> assertNotNull(bidService.findBidsForAuctionName("valerian_steal"),"a bid must be present"));
    }

    @Test
    public void test_faild_bid_on_price_requirement() throws Exception
    {
        Bidder bidder = new Bidder("Arya");
        bidderService.createItem(bidder);
        AuctionHouse auctionHouse = new AuctionHouse("HouseTargerian");
        auctionHouseService.createItem(auctionHouse);
        Auction auction = new Auction();
        auction.setName("aDragon");
        auction.setDescription("most wanted material for swards");
        auction.setStartingPrice(BigDecimal.valueOf(100));
        auction.setCurrentPrice(BigDecimal.valueOf(100));
        auction.setStartTime(LocalDateTime.MIN);
        auction.setEndTime(LocalDateTime.MAX);
        auctionService.createAuctionForAuctionHouseName("HouseTargerian",auction);

        BidForm bidForm = new BidForm();
        bidForm.setBidder("Arya");
        bidForm.setAuction("aDragon");
        bidForm.setBidPrice(BigDecimal.valueOf(99));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(bidForm);

        MvcResult result = mockMvc.perform(post("/api/bid")
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertTrue(response.contains("you can't bid lower that the current price ! please bid a higher price"));
    }

    @Test
    public void test_faild_bid_on_before_time_requirement() throws Exception
    {
        Bidder bidder = new Bidder("Arya");
        bidderService.createItem(bidder);
        AuctionHouse auctionHouse = new AuctionHouse("HouseTargerian");
        auctionHouseService.createItem(auctionHouse);
        Auction auction = new Auction();
        auction.setName("aCastle");
        auction.setDescription("most wanted material for swards");
        auction.setStartingPrice(BigDecimal.valueOf(100));
        auction.setCurrentPrice(BigDecimal.valueOf(100));
        auction.setStartTime(LocalDateTime.now().plusDays(1));
        auction.setEndTime(LocalDateTime.MAX);
        auctionService.createAuctionForAuctionHouseName("HouseTargerian",auction);

        BidForm bidForm = new BidForm();
        bidForm.setBidder("Arya");
        bidForm.setAuction("aCastle");
        bidForm.setBidPrice(BigDecimal.valueOf(101));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(bidForm);

        MvcResult result = mockMvc.perform(post("/api/bid")
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertTrue(response.contains("Auction is not yet started . please wait for the appropriate time"));
    }

    @Test
    public void test_faild_bid_on_after_time_requirement() throws Exception
    {
        Bidder bidder = new Bidder("Arya");
        bidderService.createItem(bidder);
        AuctionHouse auctionHouse = new AuctionHouse("HouseTargerian");
        auctionHouseService.createItem(auctionHouse);
        Auction auction = new Auction();
        auction.setName("anArmy");
        auction.setDescription("most wanted material for swards");
        auction.setStartingPrice(BigDecimal.valueOf(100));
        auction.setCurrentPrice(BigDecimal.valueOf(100));
        auction.setStartTime(LocalDateTime.now().minusDays(2));
        auction.setEndTime(LocalDateTime.now().minusDays(1));
        auctionService.createAuctionForAuctionHouseName("HouseTargerian",auction);

        BidForm bidForm = new BidForm();
        bidForm.setBidder("Arya");
        bidForm.setAuction("anArmy");
        bidForm.setBidPrice(BigDecimal.valueOf(101));

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(bidForm);

        MvcResult result = mockMvc.perform(post("/api/bid")
                .content(requestJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertTrue(response.contains("Auction is finished please stop bidding on this auction!"));
    }


}
