package com.spideo.submission.model;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Auction extends AbstractItem{
    private AuctionHouse auctionHouse ;

    private String name ;
    private String description ;
    private BigDecimal startingPrice ;
    private BigDecimal currentPrice ;
    private LocalDateTime startTime ;
    private LocalDateTime endTime ;

    private List<Bid> bids = new ArrayList<>();


    public Auction(AuctionHouse auctionHouse, String name, String description, BigDecimal startingPrice, BigDecimal currentPrice , LocalDateTime startTime , LocalDateTime endTime) {

        this.auctionHouse = auctionHouse;
        this.name = name;
        this.description = description;
        this.startingPrice = startingPrice;
        this.currentPrice = currentPrice;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Auction() {

    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    public AuctionHouse getAuctionHouse() {
        return auctionHouse;
    }

    public void setAuctionHouse(AuctionHouse auctionHouse) {
        this.auctionHouse = auctionHouse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public AuctionStatus getStatus ()
    {
        if (LocalDateTime.now().isAfter(endTime))
            return AuctionStatus.FINISHED;
        else if (LocalDateTime.now().isBefore(startTime))
            return AuctionStatus.NOT_STARTED;
        else
            return AuctionStatus.RUNNING;
    }
}
