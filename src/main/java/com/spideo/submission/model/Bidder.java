package com.spideo.submission.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Bidder extends AbstractItem {

    private String name ;

    @JsonIgnore
    private List<Bid> bids = new ArrayList<>();

    public Bidder(String name) {
        this.name = name;
    }

    public Bidder() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }
}
