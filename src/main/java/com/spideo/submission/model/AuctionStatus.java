package com.spideo.submission.model;

public enum AuctionStatus {

    NOT_STARTED,RUNNING,FINISHED
}
