package com.spideo.submission.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class AuctionHouse extends AbstractItem {

    private String name ;

    @JsonIgnore
    private List <Auction> auctions = new ArrayList<>();

    public AuctionHouse (String name )
    {
        this.name = name;
    }

    public AuctionHouse() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }
}
