package com.spideo.submission.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Bid extends AbstractItem {

    private Bidder bidder;
    private Auction auction;
    private LocalDateTime bidDateTime ;
    private BigDecimal bidPrice ;

    public Bid(Bidder bidder, Auction auction, LocalDateTime bidDateTime, BigDecimal bidPrice) {
        this.bidder = bidder;
        this.auction = auction;
        this.bidDateTime = bidDateTime;
        this.bidPrice = bidPrice;
    }

    public Bid() {
    }

    public Bidder getBidder() {
        return bidder;
    }

    public void setBidder(Bidder bidder) {
        this.bidder = bidder;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public LocalDateTime getBidDateTime() {
        return bidDateTime;
    }

    public void setBidDateTime(LocalDateTime bidDateTime) {
        this.bidDateTime = bidDateTime;
    }

    public BigDecimal getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(BigDecimal bidPrice) {
        this.bidPrice = bidPrice;
    }
}
