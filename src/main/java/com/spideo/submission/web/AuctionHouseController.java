package com.spideo.submission.web;

import com.spideo.submission.converter.AuctionHouseConverter;
import com.spideo.submission.converter.AuctionHouseFormConverter;
import com.spideo.submission.dto.AuctionHouseDTO;
import com.spideo.submission.dto.AuctionHouseForm;
import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.AuctionHouse;
import com.spideo.submission.service.IAuctionHouseService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/auction-house")
@SuppressWarnings("unused")
public class AuctionHouseController {

    @Resource
    private IAuctionHouseService auctionHouseService;

    @Resource
    private AuctionHouseConverter auctionHouseConverter;

    @Resource
    private AuctionHouseFormConverter auctionHouseFormConverter;

    @GetMapping
    public List <AuctionHouseDTO> getAuctionHouses ()
    {
        List auctionHouses = auctionHouseService.getAllItems();
        return auctionHouseConverter.convertAll(auctionHouses);
    }


    @PostMapping
    public void createAnAuctionHouse (@RequestBody AuctionHouseForm auctionHouse)
    {
        AuctionHouse auctionHouseModel = auctionHouseFormConverter.revertConvert(auctionHouse);
        auctionHouseService.createItem(auctionHouseModel);
    }


    @DeleteMapping("/{name}")
    @ExceptionHandler(ItemNotFoundException.class)
    public void deleteAnAuctionHouse (@PathVariable(name = "name") String name)
    {
        auctionHouseService.deleteAuctionHouseByName(name);
    }
}
