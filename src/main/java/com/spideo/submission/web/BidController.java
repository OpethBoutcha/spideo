package com.spideo.submission.web;

import com.spideo.submission.converter.Converter;
import com.spideo.submission.dto.BidDTO;
import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.Auction;
import com.spideo.submission.model.AuctionStatus;
import com.spideo.submission.model.Bid;
import com.spideo.submission.service.IBidService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/api/bid")
@SuppressWarnings("unused")
public class BidController {

    @Resource
    private IBidService bidService;

    @Resource
    private Converter<Bid,BidDTO> bidConverter;

    @PostMapping
    public void createABid (@RequestBody BidForm bidForm)
    {
        bidService.placeBid(bidForm);
    }


    @GetMapping("/auction/{auctionName}")
    public List<BidDTO> getBidsForAuction (@PathVariable(name = "auctionName") String auctionName)
    {
        return bidConverter.convertAll(bidService.findBidsForAuctionName(auctionName));
    }

    @GetMapping("/{auctionName}/{bidderName}")
    public List<BidDTO> getBidsForAuctionAndBidder (@PathVariable(name = "auctionName") String auctionName , @PathVariable(name = "bidderName") String bidderName )
    {
        return bidConverter.convertAll(bidService.findBidsForAuctionAndBidder(auctionName,bidderName));
    }


}
