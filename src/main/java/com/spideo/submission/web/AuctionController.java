package com.spideo.submission.web;

import com.spideo.submission.converter.AuctionConverter;
import com.spideo.submission.converter.AuctionFormConverter;
import com.spideo.submission.converter.BidderConverter;
import com.spideo.submission.dto.AuctionDTO;
import com.spideo.submission.dto.AuctionForm;
import com.spideo.submission.dto.AuctionHouseDTO;
import com.spideo.submission.dto.BidderDTO;
import com.spideo.submission.model.Auction;
import com.spideo.submission.model.AuctionStatus;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IAuctionService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/auction")
@SuppressWarnings("unused")
public class AuctionController {

    @Resource
    private IAuctionService auctionService;

    @Resource
    private AuctionConverter auctionConverter;

    @Resource
    private BidderConverter bidderConverter;

    @Resource
    private AuctionFormConverter auctionFormConverter;


    @GetMapping
    public List <AuctionDTO> getAuctions ()
    {
        List auctionHouses = auctionService.getAllItems();
        return auctionConverter.convertAll(auctionHouses);
    }

    @GetMapping("/details/{name}")
    public AuctionDTO getAuctionStatusDetails (@PathVariable(name = "name") String name )
    {
        Auction auction        = auctionService.findAuctionByName(name);
        Bidder bidder          = auctionService.findWinner(name);
        AuctionDTO auctionDTO  = auctionConverter.convert(auction);
        BidderDTO bidderDTO    = bidderConverter.convert(bidder);
        auctionDTO.setWinner(bidderDTO);
        return auctionDTO;
    }

    @GetMapping("/status/{status}")
    public List <AuctionHouseDTO> getAuctionHouses(@PathVariable(name = "status") AuctionStatus status)
    {
        List auctionHouses = auctionService.findAuctionByStatus(status);
        return auctionConverter.convertAll(auctionHouses);
    }

    @PostMapping("/auction-house-name/{auctionHouseName}")
    public void createAnAuction (@RequestBody AuctionForm auction , @PathVariable(name = "auctionHouseName") String auctionHouseName)
    {
        Auction auctionModel = auctionFormConverter.revertConvert(auction);
        auctionService.createAuctionForAuctionHouseName(auctionHouseName,auctionModel);
    }


    @DeleteMapping("/{name}")
    public void deleteAnAuction (@PathVariable(name = "name") String auctionName)
    {
        auctionService.deleteAuctionByName(auctionName);
    }
}
