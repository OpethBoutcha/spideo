package com.spideo.submission.web;

import com.spideo.submission.converter.BidderConverter;
import com.spideo.submission.converter.BidderFormConverter;
import com.spideo.submission.dto.AuctionDTO;
import com.spideo.submission.dto.BidderForm;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IBidderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/bidder")
@SuppressWarnings("unused")
public class BidderController {

    @Resource
    private IBidderService bidderService;

    @Resource
    private BidderConverter bidderConverter;

    @Resource
    private BidderFormConverter bidderFormConverter;


    @GetMapping
    public List<AuctionDTO> getBidders ()
    {
        List auctionHouses = bidderService.getAllItems();
        return bidderConverter.convertAll(auctionHouses);
    }

    @PostMapping
    public void createABidder (@RequestBody BidderForm bidderForm)
    {
        Bidder bidder = bidderFormConverter.revertConvert(bidderForm);
        bidderService.createItem(bidder);
    }

    @DeleteMapping("/{name}")
    public void deleteABidder (@PathVariable(name = "name") String bidder)
    {
        bidderService.deleteBidderByName(bidder);
    }
}
