package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionHouseDTO;
import com.spideo.submission.model.AuctionHouse;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AuctionHouseConverter extends Converter<AuctionHouse, AuctionHouseDTO> {

    @Resource
    ModelMapper mapper ;

    @Override
    public AuctionHouseDTO convert(AuctionHouse source) {
        return mapper.map(source,AuctionHouseDTO.class);
    }

    @Override
    public AuctionHouse revertConvert(AuctionHouseDTO source) {
         return mapper.map(source,AuctionHouse.class);
    }
}
