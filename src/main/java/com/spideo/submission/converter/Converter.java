package com.spideo.submission.converter;


import java.util.List;
import java.util.stream.Collectors;

public abstract class Converter<T,V> {

    public abstract V convert (T source) ;

    public abstract T revertConvert (V source);

    public List<V> convertAll (List<T> sourceList)
    {
        return sourceList.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

}
