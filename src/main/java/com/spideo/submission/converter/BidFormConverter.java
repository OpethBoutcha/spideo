package com.spideo.submission.converter;

import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.Bid;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BidFormConverter extends Converter<Bid, BidForm>{

    @Resource
    ModelMapper mapper ;

    @Override
    public BidForm convert(Bid source) {
        return  mapper.map(source, BidForm.class);
    }

    @Override
    public Bid revertConvert(BidForm source) {
        return  mapper.map(source, Bid.class);
    }
}
