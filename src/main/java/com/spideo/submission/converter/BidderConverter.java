package com.spideo.submission.converter;

import com.spideo.submission.dto.BidderDTO;
import com.spideo.submission.model.Bidder;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BidderConverter extends Converter<Bidder, BidderDTO> {

    @Resource
    ModelMapper mapper ;

    @Override
    public BidderDTO convert(Bidder source) {
        return  mapper.map(source, BidderDTO.class);
    }

    @Override
    public Bidder revertConvert(BidderDTO source) {
        return mapper.map(source, Bidder.class);
    }
}
