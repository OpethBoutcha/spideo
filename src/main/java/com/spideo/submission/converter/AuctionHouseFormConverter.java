package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionHouseForm;
import com.spideo.submission.model.AuctionHouse;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AuctionHouseFormConverter extends Converter<AuctionHouse, AuctionHouseForm> {

    @Resource
    ModelMapper mapper ;

    @Override
    public AuctionHouseForm convert(AuctionHouse source) {
        return mapper.map(source,AuctionHouseForm.class);
    }

    @Override
    public AuctionHouse revertConvert(AuctionHouseForm source)  {
        return mapper.map(source,AuctionHouse.class);
    }
}
