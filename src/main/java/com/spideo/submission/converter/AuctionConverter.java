package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionDTO;
import com.spideo.submission.model.Auction;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AuctionConverter extends Converter<Auction, AuctionDTO> {

    @Autowired
    ModelMapper mapper ;

    @Override
    public AuctionDTO convert(Auction source) {
        return mapper.map(source,AuctionDTO.class);
    }

    @Override
    public Auction revertConvert(AuctionDTO source) {
         return mapper.map(source,Auction.class);
    }
}
