package com.spideo.submission.converter;

import com.spideo.submission.dto.AuctionForm;
import com.spideo.submission.model.Auction;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuctionFormConverter extends Converter<Auction, AuctionForm> {

    @Autowired
    private ModelMapper mapper ;


    @Override
    public AuctionForm convert(Auction source) {
        return mapper.map(source,AuctionForm.class);
    }

    @Override
    public Auction revertConvert(AuctionForm source) {
        return mapper.map(source,Auction.class);
    }
}
