package com.spideo.submission.converter;

import com.spideo.submission.dto.BidderForm;
import com.spideo.submission.model.Bidder;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BidderFormConverter extends Converter<Bidder, BidderForm> {

    @Resource
    ModelMapper mapper ;

    @Override
    public BidderForm convert(Bidder source) {
        return mapper.map(source, BidderForm.class);
    }

    @Override
    public Bidder revertConvert(BidderForm source) {
        return mapper.map(source, Bidder.class);
    }
}
