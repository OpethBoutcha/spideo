package com.spideo.submission.converter;

import com.spideo.submission.dto.BidDTO;
import com.spideo.submission.model.Bid;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BidConverter extends Converter<Bid, BidDTO> {

    @Resource
    ModelMapper mapper ;

    @Override
    public BidDTO convert(Bid source) {
        return  mapper.map(source, BidDTO.class);
    }

    @Override
    public Bid revertConvert(BidDTO source) {
        return mapper.map(source, Bid.class);
    }
}
