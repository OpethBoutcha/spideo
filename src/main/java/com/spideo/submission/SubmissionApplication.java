package com.spideo.submission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubmissionApplication {

	public static void main(String[] args) {
		try {
			SpringApplication.run(SubmissionApplication.class, args);
		}
		catch (Exception e )
		{
			e.printStackTrace();
		}
	}

}
