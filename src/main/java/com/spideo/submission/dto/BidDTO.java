package com.spideo.submission.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BidDTO extends AbstractItemDTO {

    @JsonIgnore
    private BidderDTO bidder;
    @JsonIgnore
    private AuctionDTO auction;
    private LocalDateTime bidDateTime ;
    private BigDecimal bidPrice ;


    public BidderDTO getBidder() {
        return bidder;
    }

    public void setBidder(BidderDTO bidder) {
        this.bidder = bidder;
    }

    public AuctionDTO getAuction() {
        return auction;
    }

    public void setAuction(AuctionDTO auction) {
        this.auction = auction;
    }

    public LocalDateTime getBidDateTime() {
        return bidDateTime;
    }

    public void setBidDateTime(LocalDateTime bidDateTime) {
        this.bidDateTime = bidDateTime;
    }

    public BigDecimal getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(BigDecimal bidPrice) {
        this.bidPrice = bidPrice;
    }
}
