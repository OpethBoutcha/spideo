package com.spideo.submission.dto;

import java.util.ArrayList;
import java.util.List;

public class AuctionHouseDTO extends AbstractItemDTO {

    private String name ;
    private List <AuctionDTO> auctions;

    public String getName() {
        return name;
    }
    public List<AuctionDTO> getAuctions() {
        return auctions;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuctions(List<AuctionDTO> auctions) {
        this.auctions = auctions;
    }
}
