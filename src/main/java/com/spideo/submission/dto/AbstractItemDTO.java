package com.spideo.submission.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public class AbstractItemDTO {

    private UUID id ;
    private LocalDateTime creationTime ;
    private LocalDateTime modificationTime;

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(LocalDateTime modificationTime) {
        this.modificationTime = modificationTime;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
