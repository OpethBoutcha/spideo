package com.spideo.submission.dto;

public class AuctionHouseForm {

    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
