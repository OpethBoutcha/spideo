package com.spideo.submission.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spideo.submission.model.Bidder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AuctionDTO extends AbstractItemDTO {

    @JsonIgnore
    private AuctionHouseDTO auctionHouse ;
    private String name ;
    private String description ;
    private BigDecimal startingPrice ;
    private BigDecimal currentPrice ;
    private LocalDateTime startTime ;
    private LocalDateTime endTime ;
    private List<BidDTO> bids ;
    private BidderDTO winner ;

    public BidderDTO getWinner() {
        return winner;
    }

    public void setWinner(BidderDTO winner) {
        this.winner = winner;
    }

    public List<BidDTO> getBids() {
        return bids;
    }

    public void setBids(List<BidDTO> bids) {
        this.bids = bids;
    }

    public AuctionHouseDTO getAuctionHouse() {
        return auctionHouse;
    }

    public void setAuctionHouse(AuctionHouseDTO auctionHouse) {
        this.auctionHouse = auctionHouse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
