package com.spideo.submission.service;

import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.AuctionStatus;
import com.spideo.submission.model.Bid;

import java.util.List;
import java.util.UUID;

public interface IBidService extends AtomicModelService <Bid> {

    void placeBid(BidForm bidForm);

    List<Bid> findBidsForAuction(UUID auction);

    List<Bid> findBidsForBidder(UUID bidder);

    List<Bid> findBidsForAuctionAndBidder(String auction, String bidder);

    List<Bid> findBidsForAuctionName(String auctionName);

}
