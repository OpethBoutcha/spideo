package com.spideo.submission.service;

import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.AuctionHouse;
import com.spideo.submission.model.Bid;

public interface IAuctionHouseService extends AtomicModelService <AuctionHouse> {

    void deleteAuctionHouseByName(String name);
    AuctionHouse findAuctionHouseByName(String name);

}
