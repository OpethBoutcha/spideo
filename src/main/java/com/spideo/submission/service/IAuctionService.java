package com.spideo.submission.service;

import com.spideo.submission.model.Auction;
import com.spideo.submission.model.AuctionStatus;
import com.spideo.submission.model.Bidder;

import java.util.List;
import java.util.UUID;


public interface IAuctionService extends AtomicModelService <Auction> {

    void createAuctionForAuctionHouseId(UUID auctionHouse , Auction auction);
    void createAuctionForAuctionHouseName(String name , Auction auction);
    void deleteAuctionByName(String auctionName);
    Auction findAuctionByName(String auctionName);
    Bidder findWinner(String auctionName);
    List<Auction> findAuctionByStatus(AuctionStatus status);

}
