package com.spideo.submission.service;

import java.util.List;
import java.util.UUID;

public interface AtomicModelService<T> {

    public List<T> getAllItems () ;
    public T getAnItem (UUID id);
    public void deleteAnItem (UUID id);
    public void updateAnItem (T item , UUID id);
    public void createItem (T item);
}
