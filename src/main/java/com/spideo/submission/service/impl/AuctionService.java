package com.spideo.submission.service.impl;

import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.*;
import com.spideo.submission.service.IAuctionHouseService;
import com.spideo.submission.service.IAuctionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuctionService extends AbstractItemService<Auction> implements IAuctionService {

    private Map<UUID, Auction> auctions ;

    public AuctionService()
    {
        auctions = new HashMap<>();
    }

    @Resource
    private IAuctionHouseService auctionHouseService;

    @Override
    protected Map<UUID, Auction> getdataMap() {
        return auctions;
    }

    @Override
    public void createAuctionForAuctionHouseId(UUID auctionHouse , Auction auction) {

        AuctionHouse auctionHouseModel = auctionHouseService.getAnItem(auctionHouse);
        auction.setAuctionHouse(auctionHouseModel);
        this.createItem(auction);
        auctionHouseModel.getAuctions().add(auction);
        auctionHouseService.updateAnItem(auctionHouseModel,auctionHouseModel.getId());
    }

    @Override
    public void createAuctionForAuctionHouseName(String name , Auction auction) {

        AuctionHouse auctionHouseModel = auctionHouseService.findAuctionHouseByName(name);
        auction.setAuctionHouse(auctionHouseModel);
        this.createItem(auction);
        auctionHouseModel.getAuctions().add(auction);
        auctionHouseService.updateAnItem(auctionHouseModel,auctionHouseModel.getId());

    }

    @Override
    public void deleteAuctionByName(String auctionName) {

        Auction toDelete = findAuctionByName(auctionName);
        this.deleteAnItem(toDelete.getId());
    }

    @Override
    public Auction findAuctionByName(String auctionName) {
        return auctions.values()
                    .stream()
                    .filter(a -> a.getName().equals(auctionName))
                    .findFirst()
                    .orElseThrow(ItemNotFoundException::new);
    }

    @Override
    public List<Auction> findAuctionByStatus(AuctionStatus status) {
        return auctions
                .values()
                .stream()
                .filter( a-> a.getStatus().equals(status))
                .collect(Collectors.toList());
    }

    @Override
    public Bidder findWinner(String auctionName) {

        Auction auction = findAuctionByName(auctionName);
        Bid bid = null;
        if (AuctionStatus.FINISHED.equals(auction.getStatus()))
        {
           bid = auction.getBids().stream().max(Comparator.comparing(Bid::getBidPrice)).get();
        }
        return bid != null ? bid.getBidder() : null ;
    }
}
