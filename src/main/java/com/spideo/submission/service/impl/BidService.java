package com.spideo.submission.service.impl;

import com.spideo.submission.dto.BidForm;
import com.spideo.submission.exception.AuctionFinishedException;
import com.spideo.submission.exception.AuctionNotStartedException;
import com.spideo.submission.exception.BidPriceException;
import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.Auction;
import com.spideo.submission.model.AuctionStatus;
import com.spideo.submission.model.Bid;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IAuctionService;
import com.spideo.submission.service.IBidService;
import com.spideo.submission.service.IBidderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class BidService  extends AbstractItemService<Bid> implements IBidService {

    private Map<UUID, Bid> bids ;

    public BidService ()
    {
        bids = new HashMap<>();
    }

    @Resource
    private IAuctionService auctionService;

    @Resource
    private IBidderService bidderService;

    @Override
    protected Map<UUID, Bid> getdataMap() {
        return bids;
    }

    @Override
    public void placeBid(BidForm bidForm) {

      Bidder  bidder  = bidderService.findBidderByName(bidForm.getBidder());
      Auction auction = auctionService.findAuctionByName(bidForm.getAuction());
      validatePrice (auction.getCurrentPrice() , bidForm.getBidPrice());
      validateStatus(auction.getStatus());
      Bid bid = new Bid(bidder , auction , LocalDateTime.now() , bidForm.getBidPrice());
      this.createItem(bid);
      auction.setCurrentPrice(bidForm.getBidPrice());
      auction.getBids().add(bid);
      auctionService.updateAnItem(auction,auction.getId());
      bidder.getBids().add(bid);
      bidderService.updateAnItem(bidder,bidder.getId());
    }

    public void validateStatus(AuctionStatus status) {
        if (AuctionStatus.FINISHED.equals(status))
            throw new AuctionFinishedException();
        else if (AuctionStatus.NOT_STARTED.equals(status))
            throw new AuctionNotStartedException();
    }

    public void validatePrice(BigDecimal currentPrice, BigDecimal bidPrice) {
        if (bidPrice.compareTo(currentPrice) < 1)
        {
            throw new BidPriceException();
        }
    }

    @Override
    public List<Bid> findBidsForAuction(UUID auction) {
        return auctionService.getAnItem(auction).getBids();
    }

    @Override
    public List<Bid> findBidsForBidder(UUID bidder) {
        return bidderService.getAnItem(bidder).getBids();
    }

    @Override
    public List<Bid> findBidsForAuctionAndBidder(String auction, String bidder) {
        List<Bid> allBids = this.getAllItems();
        return allBids.stream()
                .filter(b -> b.getAuction().getName().equals(auction) && b.getBidder().getName().equals(bidder))
                .collect(Collectors.toList());
    }

    @Override
    public List<Bid> findBidsForAuctionName(String auctionName) {
        return auctionService
                .findAuctionByName(auctionName)
                .getBids();
    }

}
