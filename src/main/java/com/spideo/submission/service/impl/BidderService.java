package com.spideo.submission.service.impl;

import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.IBidderService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class BidderService extends AbstractItemService <Bidder> implements IBidderService {

    private Map<UUID, Bidder> bidders ;

    public BidderService ()
    {
        bidders = new HashMap<>();
    }

    @Override
    protected Map<UUID, Bidder> getdataMap() {
        return bidders;
    }

    @Override
    public Bidder findBidderByName(String bidderName) {
        return bidders.values().stream()
                .filter(b-> b.getName().equals(bidderName))
                .findFirst()
                .orElseThrow(ItemNotFoundException::new);
    }

    @Override
    public void deleteBidderByName(String bidder) {
        Bidder bidderModel  = findBidderByName(bidder);
        this.deleteAnItem(bidderModel.getId());
    }
}
