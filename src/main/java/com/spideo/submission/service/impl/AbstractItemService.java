package com.spideo.submission.service.impl;

import com.spideo.submission.model.AbstractItem;
import com.spideo.submission.service.AtomicModelService;

import java.time.LocalDateTime;
import java.util.*;

public abstract class AbstractItemService <T extends AbstractItem>   implements AtomicModelService<T> {

    protected abstract Map<UUID,T> getdataMap () ;

    @Override
    public List<T> getAllItems() {
        return new ArrayList<>(getdataMap().values());
    }

    @Override
    public T getAnItem(UUID id) {
        return getdataMap().get(id);
    }

    @Override
    public void deleteAnItem(UUID id) {
       getdataMap().remove(id);
    }

    @Override
    public void updateAnItem(T item, UUID id) {
        item.setModificationTime(LocalDateTime.now());
        getdataMap().replace(id,item);
    }

    @Override
    public void createItem(T item) {
        UUID id = generateUniqueId();
        item.setId(id);
        item.setCreationTime(LocalDateTime.now());
        item.setModificationTime(LocalDateTime.now());
        getdataMap().put(id,item);
    }

    protected synchronized UUID generateUniqueId ()
    {
        return UUID.randomUUID();
    }
}
