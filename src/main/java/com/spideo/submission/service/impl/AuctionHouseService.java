package com.spideo.submission.service.impl;

import com.spideo.submission.exception.ItemNotFoundException;
import com.spideo.submission.model.AuctionHouse;
import com.spideo.submission.service.IAuctionHouseService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class AuctionHouseService extends AbstractItemService<AuctionHouse> implements IAuctionHouseService {

    private Map<UUID, AuctionHouse> auctionHouses ;

    public AuctionHouseService ()
    {
        auctionHouses = new HashMap<>();
    }

    @Override
    protected Map<UUID, AuctionHouse> getdataMap() {
        return auctionHouses;
    }

    @Override
    public void deleteAuctionHouseByName(String name) {

        AuctionHouse toDelete = auctionHouses.values()
                .stream()
                .filter(a -> a.getName().equals(name))
                .findFirst()
                .orElseThrow(ItemNotFoundException::new);

        this.deleteAnItem(toDelete.getId());
    }

    @Override
    public AuctionHouse findAuctionHouseByName(String name) {
        return this.getAllItems()
                .stream()
                .filter(a -> a.getName().equals(name))
                .findFirst()
                .orElseThrow(ItemNotFoundException::new);
    }
}
