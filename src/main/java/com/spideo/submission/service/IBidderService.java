package com.spideo.submission.service;

import com.spideo.submission.dto.BidForm;
import com.spideo.submission.model.Bid;
import com.spideo.submission.model.Bidder;
import com.spideo.submission.service.AtomicModelService;

public interface IBidderService extends AtomicModelService<Bidder> {

    public Bidder findBidderByName(String bidderName);
    void deleteBidderByName(String bidder);
}
