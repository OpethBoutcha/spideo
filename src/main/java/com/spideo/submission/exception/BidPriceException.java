package com.spideo.submission.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Price too low")
public class BidPriceException extends SpideoRestException {

    private static final long serialVersionUID = 1L;

    public BidPriceException()
    {
        super("you can't bid lower that the current price ! please bid a higher price");
    }

}