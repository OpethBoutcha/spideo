package com.spideo.submission.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Auction finished")
public class AuctionFinishedException  extends SpideoRestException{

    public AuctionFinishedException() {
        super("Auction is finished please stop bidding on this auction!");
    }
}
