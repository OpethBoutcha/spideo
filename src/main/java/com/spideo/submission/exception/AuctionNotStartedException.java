package com.spideo.submission.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Auction not started")
public class AuctionNotStartedException extends SpideoRestException {

    public AuctionNotStartedException() {
        super("Auction is not yet started . please wait for the appropriate time");
    }
}
