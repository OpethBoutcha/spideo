package com.spideo.submission.exception;

public class SpideoRestException extends RuntimeException {
    public SpideoRestException(String message) {
        super(message);
    }
}
