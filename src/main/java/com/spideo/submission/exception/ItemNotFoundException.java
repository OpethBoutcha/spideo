package com.spideo.submission.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ItemNotFoundException extends SpideoRestException {

    private static final long serialVersionUID = 1L;

    public ItemNotFoundException()
    {
         super("Item not found ! please make sure you enter a valid item");
    }

}
