package com.spideo.submission.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;


@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = { SpideoRestException.class})
    protected ResponseEntity<Object> handleSpideoRestException(
            RuntimeException ex, WebRequest request) {

        HttpStatus status = getHttpStatusFromException(ex);
        ApiError error = new ApiError();
        error.setTimestamp(LocalDateTime.now());
        error.setMessage(ex.getMessage());
        error.setStatus(status);
        return handleExceptionInternal(ex, error,
                new HttpHeaders(), status, request);
    }

    private HttpStatus getHttpStatusFromException(RuntimeException ex) {

        return ex.getClass().getAnnotation(ResponseStatus.class).code();
    }
}
