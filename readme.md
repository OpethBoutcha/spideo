# Project overview

this is a submission for Spideo job vacancy . The submission concists of creating an auction house service .

# Technologies used 

- Java(8)
- Gradle(6.4.1)
- Spring boot(2.3.1.RELEASE)

# Setting up the project on a local machine 

## getting the project from repository 
- clone the project locally using HTTP : git clone https://OpethBoutcha@bitbucket.org/OpethBoutcha/spideo.git

## importing the project to Intellij 
- to open project on Intellij please refer to : [documentation](https://www.jetbrains.com/help/idea/gradle.html#gradle_jvm)

## running the project locally 

- if project imported successfully running the main class : com.spideo.submission.SubmissionApplication will run the project
- alternatively project can be launched using command line :
  - in windows : gradlew.bat bootRun
  - in linux   : ./gradlew bootRun

if running the project was successful the server should be started on port 8080

## running the tests locally 

- Using IDE running tests is done through right clicking on test folder and then clicking on "Run tests"

## Model design for the submission 

- Bidder       : is the person who bids on an auction 
- Bid          : is the action made by a bidder on auction
- Auction      : is a sale fo goods or property
- AuctionHouse : is a group of auctions 

more info on the model and the attributes cane be seen either in the package : com.spideo.submission.model or by checking the [swagger page](http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config) when the server is started


## The process to make a successful bid 

- create an auction house : http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/auction-house-controller/createAnAuctionHouse
- create an auction and link it to the previous auction house : http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/auction-controller/createAnAuction
- create a bidder using : http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/bidder-controller/createABidder
- to bid on auction  use the link : http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/bid-controller/createABid 
  - use the bidder name and the auction name 
  
the scenario can also be seen by looking at the class test : com.spideo.submission.web.BidControllerTest  


# Important notes on the submission

there is still more work to do on this submission : 

- logging 
- More tests (even though the existing ones cover the most important scenarios )
- CI/CD to build and deploy the application
- Internationalisation of error messages 

 




